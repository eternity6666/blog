---
title : "HDOJ2037今年暑假不AC"
category : 
- ACM
- HDOJ
- C++
tags :
- 贪心
date : 2018-09-28
author : 达芬奇的画笔
description : HDOJ2037今年暑假不AC题解,算法思想:贪心
---

<!-- more -->
<center><a href="http://acm.hdu.edu.cn/showproblem.php?pid=2037">今年暑假不AC</a></center>

<center>
Time Limit: 2000/1000 MS (Java/Others)    Memory Limit: 65536/32768 K (Java/Others)
Total Submission(s): 75518    Accepted Submission(s): 40611
</center>

# Problem Description
“今年暑假不AC？”
“是的。”
“那你干什么呢？”
“看世界杯呀，笨蛋！”
“@#$%^&*%...”

确实如此，世界杯来了，球迷的节日也来了，估计很多ACMer也会抛开电脑，奔向电视了。
作为球迷，一定想看尽量多的完整的比赛，当然，作为新时代的好青年，你一定还会看一些其它的节目，比如新闻联播（永远不要忘记关心国家大事）、非常6+7、超级女生，以及王小丫的《开心辞典》等等，假设你已经知道了所有你喜欢看的电视节目的转播时间表，你会合理安排吗？（目标是能看尽量多的完整节目）

# Input
输入数据包含多个测试实例，每个测试实例的第一行只有一个整数n(n<=100)，表示你喜欢看的节目的总数，然后是n行数据，每行包括两个数据Ti_s,Ti_e (1<=i<=n)，分别表示第i个节目的开始和结束时间，为了简化问题，每个时间都用一个正整数表示。n=0表示输入结束，不做处理。

# Output
对于每个测试实例，输出能完整看到的电视节目的个数，每个测试实例的输出占一行。

# Sample Input
```bash
12
1 3
3 4
0 7
3 8
15 19
15 20
10 15
8 18
6 12
5 10
4 14
2 9
0
```

# Sample Output
```bash
5
```

# Author
lcy
 
# Source
ACM程序设计期末考试（2006/06/07）

# Code
## 错误的代码:
思路:将结构体数组按照播放时长从小到大排序
```C++
#include <bits/stdc++.h>
#define fei(a, b) for(int i = a; i <= b; i++)
#define fej(a, b) for(int j = a; j <= b; j++)
#define fni(a, b) for(int i = a; i < b; i++)
#define fnj(a, b) for(int j = a; j < b; j++)
using namespace std;

const int maxn = 100 + 5;
int n;
struct node{
    int s, e, length;
}program[maxn];
bool used[25];
bool cmp(const node a, const node b)
{
    if(a.length != b.length)
        return a.length < b.length;
    else if(a.e != b.e)
        return a.e < b.e;
    else
        return a.s > b.s;
}

int main()
{
    // freopen("in.txt", "r", stdin);
    while(~scanf("%d", &n) && n)
    {
        memset(used, 1, sizeof(used));
        fei(1, n)
        {
            cin >> program[i].s >> program[i].e;
            program[i].length = program[i].e - program[i].s;
        }
        sort(program + 1, program + n + 1, cmp);
        int ans = 0;
        fei(1, n)
        {
            bool flag = 1;
            fej(program[i].s + 1, program[i].e - 1)
            {
                if(used[j] == 0)
                {
                    flag = 0;
                    break;
                }
            }
            if(flag)
            {
                // cout << program[i].s << program[i].e << endl;
                ans++;
                fej(program[i].s + 1, program[i].e - 1)
                    used[j] = 0;
            }
        }
        cout << ans << endl;
    }
    return 0;
}
```

## 正确的代码:
思路:将结构体数组按照结束时间先后从小到大排序
```C++
#include <bits/stdc++.h>
#define fei(a, b) for(int i = a; i <= b; i++)
#define fej(a, b) for(int j = a; j <= b; j++)
#define fni(a, b) for(int i = a; i < b; i++)
#define fnj(a, b) for(int j = a; j < b; j++)
using namespace std;

const int maxn = 100 + 5;
int n;
struct node{
    int s, e;
}program[maxn];
bool used[25];
bool cmp(const node a, const node b)
{
    return a.e < b.e;
}

int main()
{
    // freopen("in.txt", "r", stdin);
    while(~scanf("%d", &n) && n)
    {
        memset(used, 1, sizeof(used));
        fei(1, n)
        {
            cin >> program[i].s >> program[i].e;
        }
        sort(program + 1, program + n + 1, cmp);
        int ans = 1;
        int tmp = program[1].e;
        fei(2, n)
        {
            if(program[i].s >= tmp)
            {
                ans++;
                tmp = program[i].e;
            }
        }
        cout << ans << endl;
    }
    return 0;
}
```